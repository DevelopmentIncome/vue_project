import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import login from '@/pages/login/login'
import userManage from '@/pages/userManage/userManage'
import institutionManage from '@/pages/institutionManage/institutionManage'
import currencyTransactionDetails from '@/pages/currencyTransactionDetails/currencyTransactionDetails'
import customerServiceManage from '@/pages/customerServiceManage/customerServiceManage'
import menuManage from '@/pages/menuManage/menuManage'
import legalCurrencyTransactionDetails from '@/pages/legalCurrencyTransactionDetails/legalCurrencyTransactionDetails'
import operationAccount from '@/pages/operationAccount/operationAccount'
import parameterSetting from '@/pages/parameterSetting/parameterSetting'
import privilegesManage from '@/pages/privilegesManage/privilegesManage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: '登录',
      component: login,
      isHidden: true,
    },
    {
      path: '',
      name: '系统管理',
      component: Home,
      children: [
        {
          path: '/operationAccount',
          name: '运营账号管理',
          component: operationAccount,
        },
        {
          path: '/userManage',
          name: '用户管理',
          component: userManage,
        },
        {
          path: '/privilegesManage',
          name: '角色权限管理',
          component: privilegesManage,
        },
        {
          path: '/menuManage',
          name: '菜单管理',
          component: menuManage,
        },
        {
          path: '/institutionManage',
          name: '部门管理',
          component: institutionManage,
        },
      ]
    },
    {
      path: '',
      name: '交易明细',
      component: Home,
      children: [
        {
          path: '/legalCurrencyTransactionDetails',
          name: '法币交易明细',
          component: legalCurrencyTransactionDetails,
        },
        {
          path: '/currencyTransactionDetails',
          name: '币币交易明细',
          component: currencyTransactionDetails,
        },{
          path: '/customerServiceManage',
          name: '客诉管理',
          component: customerServiceManage,
        },
        {
          path: '/parameterSetting',
          name: '基础参数设置',
          component: parameterSetting,
        },
      ]
    },

  ]
})
