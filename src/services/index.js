//http://47.75.114.151:8080/swagger-ui.html
//http://wangliang.vipgz1.idcfengye.com
//http://wangliang008.vipgz1.idcfengye.com
var host = 'http://wangliang008.vipgz1.idcfengye.com/api'
export default{
  login:{url:host+'/user/login'},
  menuList:{url:host+'/roleMenuMiddle/selectById'},
  userList:{url:host+'/user/selectusers'},
  addUser:{url:host+'/user/insert'},
  deleteUser:{url:host+'/user/delete'},
  updateUser:{url:host+'/user/update'},
  roleList:{url:host+'/role/list'},
  addRole:{url:host+'/role/insert'},
  deleteRole:{url:host+'/role/delete'},
  updateRole:{url:host+'/role/update'},
  selectBranch:{url:host+'/institution/selectOne'},
  selectRole:{url:host+'/institution/selectOne'},
  selectById:{url:host+'/user/selectById'},
  paramsList:{url:host+'/syspara/selectsyspara'},
  tradeList:{url:host+'/trade/selecttrade'},
  searchTradeList:{url:host+'/trade/findTrade'},
  customerList:{url:host+'/question/selectquestion'},
  searchCustomerList:{url:host+'/questions/findquestions'},
  getMenuList:{url:host+'/menu/list'},
  getGradeList:{url:host+'/menu/Grade'},
  addMenuList:{url:host+'/menu/insertMenu'},
  editMenuList:{url:host+'/menu/update'},
  deleteMenuList:{url:host+'/menu/deleteById'},
  getLower:{url:host+'/menu/getLower'},
  roleMenu:{url:host+'/roleMenuMiddle/selectById'},
  addRoleMenu:{url:host+'/roleMenu/insert'},
  getinstitutionList:{url:host+'/institution/getLower1'},
  searchInstitution:{url:host+'/institution/select'},
  insertInstitution:{url:host+'/institution/insertSame'},
  deleteInstitution:{url:host+'/institution/delete'},
}

